all: transport network

transport: obj/transport.o obj/string_z.o
	mkdir -p bin
	gcc -pthread -o bin/$@ $^

obj/transport.o: src/transport.c src/include/app_data.h src/include/app_connection.h src/include/app_response.h src/include/err.h src/include/primitive.h src/include/disconnect.h src/include/fifo.h src/include/simpleq_entry.h
	mkdir -p obj
	gcc -o $@ -g -c $<

network: obj/network.o obj/string_z.o
	mkdir -p bin
	gcc -pthread -o bin/$@ $^

obj/network.o: src/network.c src/include/err.h src/include/primitive.h src/include/packet.h src/include/disconnect.h src/include/fifo.h src/include/network_connection.h src/include/simpleq_entry.h
	mkdir -p obj
	gcc -o $@ -g -c $<


obj/string_z.o: src/string_z.c src/include/string_z.h
	mkdir -p obj
	gcc -o $@ -g -c $<

clean :
	rm -r bin
	rm -r obj
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/queue.h>

#include "include/app_data.h"
#include "include/app_connection.h"
#include "include/app_response.h"

#include "include/err.h"
#include "include/fifo.h"
#include "include/primitive.h"
#include "include/disconnect.h"
#include "include/string_z.h"
#include "include/simpleq_entry.h"

// Session filenames
#define S_READ "S_lec"
#define S_WRITE "S_ecr"

#define CONNECTIONS_LENGTH 256
#define ADDRESS_MOD 250

// Global declarations
struct app_connection* connections[CONNECTIONS_LENGTH];
pthread_mutex_t connections_mutex[CONNECTIONS_LENGTH];

SIMPLEQ_HEAD(listhead, simpleq_entry) head = SIMPLEQ_HEAD_INITIALIZER(head);
pthread_mutex_t head_mutex;

int network_fifo_fd;
pthread_mutex_t network_write_mutex;
pthread_mutex_t session_write_mutex;

// Function prototypes
void read_from_session();
void read_from_network();

void write_to_network(void* data, size_t length);
void write_to_session(void* data, size_t length);

void destroy_connection(unsigned char id);

void handle_app_data(struct app_data* app_data);
void handle_connect_conf_primitive(struct connect_conf_primitive* connect_conf_primitive);
void handle_disconnect_ind_primitive(struct disconnect_ind_primitive* disconnect_ind_primitive);

void insert_simpleq_entry(struct simpleq_entry* entry);

int main() {
    // Initialize random seed
    srand(time(NULL));

    // Mutex initalization
    for (size_t i = 0; i < CONNECTIONS_LENGTH; i++) {
        pthread_mutex_init(&connections_mutex[i], NULL);
    }
    pthread_mutex_init(&head_mutex, NULL);
    pthread_mutex_init(&network_write_mutex, NULL);
    pthread_mutex_init(&session_write_mutex, NULL);

    // Open network FIFO
    network_fifo_fd = open(NETWORK_FIFO, O_WRONLY);
    if (network_fifo_fd == -1) {
        perror(ERR_NETWORK_FIFO_OPEN);
        return -1;
    }

    pthread_t session_tid;
    if (pthread_create(&session_tid, NULL, read_from_session, NULL) == -1) {
        perror(ERR_PTHREAD_CREATE);
    } else {
        pthread_t network_tid;
        if (pthread_create(&network_tid, NULL, read_from_network, NULL) == -1) {
            perror(ERR_PTHREAD_CREATE);
        } else {
            // Wait for all application data to be sent
            pthread_join(session_tid, NULL);

            // Wait for all primitives to be received
            pthread_join(network_tid, NULL);

            // Wait for all threads to terminate
            while (!SIMPLEQ_EMPTY(&head)) {
                struct simpleq_entry* entry = SIMPLEQ_FIRST(&head);
                SIMPLEQ_REMOVE_HEAD(&head, entries);
                pthread_join(*(pthread_t*)entry->data, NULL);
                free(entry->data);
                free(entry);
            }
        }
    }

    // Close network FIFO
    close(network_fifo_fd);

    // Mutex destruction
    for (size_t i = 0; i < CONNECTIONS_LENGTH; i++) {
        pthread_mutex_destroy(&connections_mutex[i]);
    }
    pthread_mutex_destroy(&head_mutex);
    pthread_mutex_destroy(&network_write_mutex);
    pthread_mutex_destroy(&session_write_mutex);
    return 0;
}

void read_from_session() {
    // Open session
    int s_read_fd = open(S_READ, O_RDONLY);
    if (s_read_fd == -1) {
        perror(ERR_S_READ_OPEN);
        return;
    }

    ssize_t bytes_read;
    // Create first incoming application data
    struct app_data* app_data = malloc(sizeof(struct app_data));

    // Read data from session
    while ((bytes_read = read(s_read_fd, app_data, sizeof(unsigned char))) > 0) {
        // Read null-terminated data
        app_data->data = malloc(sizeof(char));
        if ((bytes_read = read_z(s_read_fd, &app_data->data)) == -1) {
            free(app_data->data);
            break;
        }

        // Create thread
        pthread_t* tid = malloc(sizeof(pthread_t));
        if (pthread_create(tid, NULL, handle_app_data, app_data) == -1) {
            free(app_data->data);
            free(tid);

            perror(ERR_PTHREAD_CREATE);
            break;
        }

        // Create queue entry
        struct simpleq_entry* tid_entry = malloc(sizeof(struct simpleq_entry));
        tid_entry->data = tid;

        insert_simpleq_entry(tid_entry);

        // Create next incoming application data
        app_data = malloc(sizeof(struct app_data));
    }

    if (bytes_read == -1) {
        perror(ERR_S_READ_READ);
    }

    free(app_data);
    close(s_read_fd);
}

void read_from_network() {
    // Open transport
    int transport_fifo_fd = open(TRANSPORT_FIFO, O_RDONLY);
    if (transport_fifo_fd == -1) {
        perror(ERR_TRANSPORT_FIFO_OPEN);
        return;
    }

    size_t bytes_read;
    // Create incoming primitive
    struct primitive* primitive = malloc(sizeof(struct primitive));

    // Read primitive from network
    while ((bytes_read = read(transport_fifo_fd, primitive, sizeof(struct primitive))) > 0) {
        // Create thread
        pthread_t* tid = NULL;

        switch (primitive->primitive & N_MASK) {
            case N_CONNECT_CONF: {
                // Cast primitive
                struct connect_conf_primitive* connect_conf_primitive = malloc(sizeof(struct connect_conf_primitive));
                memcpy(connect_conf_primitive, primitive, sizeof(struct primitive));

                // Read response address
                if ((bytes_read = read(transport_fifo_fd, &connect_conf_primitive->resp, sizeof(unsigned char))) == -1) {
                    free(connect_conf_primitive);
                    break;
                }

                // Create thread
                tid = malloc(sizeof(pthread_t));
                if (pthread_create(tid, NULL, handle_connect_conf_primitive, connect_conf_primitive) == -1) {
                    free(connect_conf_primitive);
                    free(tid);
                    tid = NULL;

                    perror(ERR_PTHREAD_CREATE);
                    break;
                }
                break;
            }
            case N_DISCONNECT_IND: {
                // Cast primitive
                struct disconnect_ind_primitive* disconnect_ind_primitive = malloc(sizeof(struct disconnect_ind_primitive));
                memcpy(disconnect_ind_primitive, primitive, sizeof(struct primitive));

                // Read response address
                if ((bytes_read = read(transport_fifo_fd, &disconnect_ind_primitive->resp, sizeof(unsigned char))) == -1) {
                    free(disconnect_ind_primitive);
                    break;
                }

                // Read reason
                if ((bytes_read = read(transport_fifo_fd, &disconnect_ind_primitive->reason, sizeof(unsigned char))) == -1) {
                    free(disconnect_ind_primitive);
                    break;
                }

                // Create thread
                tid = malloc(sizeof(pthread_t));
                if (pthread_create(tid, NULL, handle_disconnect_ind_primitive, disconnect_ind_primitive) == -1) {
                    free(disconnect_ind_primitive);
                    free(tid);
                    tid = NULL;

                    perror(ERR_PTHREAD_CREATE);
                    break;
                }
                break;
            }
        }

        if (tid) {
            // Create queue entry
            struct simpleq_entry* tid_entry = malloc(sizeof(struct simpleq_entry));
            tid_entry->data = tid;

            insert_simpleq_entry(tid_entry);
        }
    }

    if (bytes_read == -1) {
        perror(ERR_TRANSPORT_FIFO_READ);
    }

    free(primitive);
    close(transport_fifo_fd);
}

void write_to_network(void* data, size_t length) {
    ssize_t bytes_wrote = write(network_fifo_fd, data, length);
    if (bytes_wrote == -1) {
        perror(ERR_NETWORK_FIFO_WRITE);
    }
}

void write_to_session(void* data, size_t length) {
    ssize_t bytes_wrote;
    int s_write_fd = open(S_WRITE, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (s_write_fd == -1) {
        perror(ERR_S_WRITE_OPEN);
    } else {
        bytes_wrote = write(s_write_fd, data, length);
        if (bytes_wrote == -1) {
            perror(ERR_S_WRITE_WRITE);
        }
        close(s_write_fd);
    }
}

void destroy_connection(unsigned char id) {
    if (connections[id]) {
        free(connections[id]->data);
        free(connections[id]);
        connections[id] = NULL;
    }
}

void handle_app_data(struct app_data* app_data) {
    // Handle connection in a thread-safe way
    pthread_mutex_lock(&connections_mutex[app_data->id]);

    if (connections[app_data->id]) {
        // Add application data to the existing connection
        size_t existing_data_length = strlen(connections[app_data->id]->data);
        size_t app_data_length = strlen(app_data->data);
        size_t new_data_length = existing_data_length + app_data_length + 1;
        connections[app_data->id]->data = realloc(connections[app_data->id]->data, new_data_length);
        strcat(connections[app_data->id]->data, app_data->data);
    } else {
        // Create new connection
        struct app_connection* connection = malloc(sizeof(struct app_connection));
        connection->established = 0;
        connection->data = app_data->data;

        // Generate address
        connection->source = rand() % ADDRESS_MOD;

        // Ensure dest is different from source
        do {
            connection->dest = rand() % ADDRESS_MOD;
        } while (connection->source == connection->dest);

        // Set connection
        connections[app_data->id] = connection;

        // Create connection request primitive
        struct connect_req_primitive* connect_req_primitive = malloc(sizeof(struct connect_req_primitive));
        connect_req_primitive->id = app_data->id;
        connect_req_primitive->primitive = N_CONNECT_REQ;
        connect_req_primitive->source = connection->source;
        connect_req_primitive->dest = connection->dest;

        // Write to network in a thread-safe way
        pthread_mutex_lock(&network_write_mutex);
        write_to_network(connect_req_primitive, sizeof(struct connect_req_primitive));
        pthread_mutex_unlock(&network_write_mutex);
        free(connect_req_primitive);
    }

    pthread_mutex_unlock(&connections_mutex[app_data->id]);
    free(app_data);
}

void handle_connect_conf_primitive(struct connect_conf_primitive* connect_conf_primitive) {
    // Handle connection in a thread-safe way
    pthread_mutex_lock(&connections_mutex[connect_conf_primitive->id]);

    // Update connection state
    connections[connect_conf_primitive->id]->established = 1;

    // Create data request primitive
    struct data_req_primitive* data_req_primitive = malloc(sizeof(struct data_req_primitive));
    data_req_primitive->id = connect_conf_primitive->id;
    data_req_primitive->primitive = N_DATA_REQ;
    data_req_primitive->data = connections[connect_conf_primitive->id]->data;

    // Copy data request primitive to buffer to remove struct 8-byte padding
    size_t data_length = strlen(data_req_primitive->data) + 1;
    char* buffer = malloc(sizeof(unsigned char) + sizeof(unsigned char) + data_length);
    size_t buffer_length = 0;

    memcpy(buffer + buffer_length, &data_req_primitive->id, sizeof(unsigned char));
    buffer_length += sizeof(unsigned char);

    memcpy(buffer + buffer_length, &data_req_primitive->primitive, sizeof(unsigned char));
    buffer_length += sizeof(unsigned char);

    memcpy(buffer + buffer_length, data_req_primitive->data, data_length);
    buffer_length += data_length;

    // Write buffer to network in a thread-safe way
    pthread_mutex_lock(&network_write_mutex);
    write_to_network(buffer, buffer_length);
    pthread_mutex_unlock(&network_write_mutex);

    free(data_req_primitive);
    free(buffer);

    // Create disconnect request primitive
    struct disconnect_req_primitive* disconnect_req_primitive = malloc(sizeof(struct disconnect_req_primitive));
    disconnect_req_primitive->id = connect_conf_primitive->id;
    disconnect_req_primitive->primitive = N_DISCONNECT_REQ;
    disconnect_req_primitive->resp = connections[connect_conf_primitive->id]->source;

    // Write to network in a thread-safe way
    pthread_mutex_lock(&network_write_mutex);
    write_to_network(disconnect_req_primitive, sizeof(struct disconnect_req_primitive));
    pthread_mutex_unlock(&network_write_mutex);

    free(disconnect_req_primitive);

    // Destroy connection
    destroy_connection(connect_conf_primitive->id);

    pthread_mutex_unlock(&connections_mutex[connect_conf_primitive->id]);
    free(connect_conf_primitive);
}

void handle_disconnect_ind_primitive(struct disconnect_ind_primitive* disconnect_ind_primitive) {
    // Handle connection in a thread-safe way
    pthread_mutex_lock(&connections_mutex[disconnect_ind_primitive->id]);

    // Create application response
    struct app_response* app_response = malloc(sizeof(struct app_response));
    app_response->id = disconnect_ind_primitive->id;
    app_response->type = S_DISCONNECT;
    app_response->reason = disconnect_ind_primitive->reason;

    pthread_mutex_lock(&session_write_mutex);
    write_to_session(app_response, sizeof(struct app_response));
    pthread_mutex_unlock(&session_write_mutex);

    free(app_response);

    // Destroy connection
    destroy_connection(disconnect_ind_primitive->id);

    pthread_mutex_unlock(&connections_mutex[disconnect_ind_primitive->id]);
    free(disconnect_ind_primitive);
}

void insert_simpleq_entry(struct simpleq_entry* entry) {
    // Insert entry to queue in a thread-safe way
    pthread_mutex_lock(&head_mutex);
    if (SIMPLEQ_EMPTY(&head)) {
        SIMPLEQ_INSERT_HEAD(&head, entry, entries);
    } else {
        SIMPLEQ_INSERT_TAIL(&head, entry, entries);
    }
    pthread_mutex_unlock(&head_mutex);
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "include/string_z.h"

ssize_t read_z(int fd, char** buf) {
    char* current;
    char next;
    ssize_t length = 1;
    ssize_t bytes_read;

    do {
        bytes_read = read(fd, &next, sizeof(char));

        switch (bytes_read) {
            case -1:
                return -1;
            case 0:
                current = *buf + length - 1;
                *current = 0;
                break;
            default:
                if (next) {
                    length += bytes_read;
                    *buf = realloc(*buf, length);
                    current = *buf + length - 2;
                } else {
                    current = *buf + length - 1;
                }

                *current = next;
                break;
        }
    } while (bytes_read > 0 && next);

    return length;
}
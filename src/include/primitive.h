// Connection primitives
#define N_CONNECT_REQ 0b0
#define N_CONNECT_IND 0b1
#define N_CONNECT_RESP 0b10
#define N_CONNECT_CONF 0b11

// Data transfer primitives
#define N_DATA_REQ 0b100
#define N_DATA_IND 0b101

// Disconnect primitives
#define N_DISCONNECT_REQ 0b110
#define N_DISCONNECT_IND 0b111

// Primitives mask
#define N_MASK 0b111

struct primitive {
    unsigned char id;
    unsigned char primitive;
};

struct connect_req_primitive {
    unsigned char id;
    unsigned char primitive;
    unsigned char source;
    unsigned char dest;
};

struct connect_conf_primitive {
    unsigned char id;
    unsigned char primitive;
    unsigned char resp;
};

struct disconnect_req_primitive {
    unsigned char id;
    unsigned char primitive;
    unsigned char resp;
};

struct disconnect_ind_primitive {
    unsigned char id;
    unsigned char primitive;
    unsigned char resp;
    unsigned char reason;
};

struct data_req_primitive {
    unsigned char id;
    unsigned char primitive;
    char* data;
};

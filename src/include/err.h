#define ERR_S_READ_OPEN "Erreur lors de l'ouverture de la session"
#define ERR_S_READ_READ "Erreur lors de la lecture de la session"

#define ERR_S_WRITE_OPEN "Erreur lors de l'ouverture de la session"
#define ERR_S_WRITE_WRITE "Erreur lors de l'écrite dans la session"

#define ERR_L_WRITE_OPEN "Erreur lors de l'ouverture de la liaison sortante"
#define ERR_L_WRITE_WRITE "Erreur lors de l'écrite dans la liaison sortante"

#define ERR_L_READ_OPEN "Erreur lors de l'ouverture de la liaison entrante"
#define ERR_L_READ_WRITE "Erreur lors de l'écrite dans la liaison entrante"

#define ERR_NETWORK_FIFO_MKFIFO "Erreur lors de la création du réseau"
#define ERR_NETWORK_FIFO_OPEN "Erreur lors de l'ouverture du réseau"
#define ERR_NETWORK_FIFO_READ "Erreur lors de la lecture du réseau"
#define ERR_NETWORK_FIFO_WRITE "Erreur lors de l'écriture dans le réseau"

#define ERR_TRANSPORT_FIFO_MKFIFO "Erreur lors de la création du transport"
#define ERR_TRANSPORT_FIFO_OPEN "Erreur lors de l'ouverture du transport"
#define ERR_TRANSPORT_FIFO_READ "Erreur lors de la lecture du transport"
#define ERR_TRANSPORT_FIFO_WRITE "Erreur lors de l'écriture dans le transport"

#define ERR_PTHREAD_CREATE "Erreur lors de la création d'un thread"
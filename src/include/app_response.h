#define S_DISCONNECT 0b1

struct app_response {
    unsigned char id;
    unsigned char type;
    unsigned char reason;
};

// Packet type
#define T_DATA 0b0
#define T_CONNECT 0b1011
#define T_ESTABLISHED 0b1111
#define T_DISCONNECT 0b00010011
#define T_ACK 0b0001
#define T_NACK 0b1001

// Data packet parameters
#define M_MASK 0b00010000
#define M_RSH 4
#define PR_MASK 0b11100000
#define PR_RSH 5
#define PS_MASK 0b1110
#define PS_RSH 1
#define PS_MOD 8

// Max data packet length
#define MAX_DATA_LENGTH 128

struct packet {
    unsigned char connection;
    unsigned char type;
};

struct connect_packet {
    unsigned char connection;
    unsigned char type;
    unsigned char source;
    unsigned char dest;
};

struct disconnect_packet {
    unsigned char connection;
    unsigned char type;
    unsigned char source;
    unsigned char dest;
    unsigned char reason;
};

struct data_packet {
    unsigned char connection;
    unsigned char type;
    char* data;
};
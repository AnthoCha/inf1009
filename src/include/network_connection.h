struct network_connection {
    unsigned char id;
    unsigned char source;
    unsigned char dest;
    unsigned char established;
};
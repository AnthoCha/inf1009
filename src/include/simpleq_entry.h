#include <pthread.h>
#include <sys/queue.h>

struct simpleq_entry {
    void* data;
    SIMPLEQ_ENTRY(simpleq_entry) entries;
};
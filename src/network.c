#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/queue.h>

#include "include/err.h"
#include "include/fifo.h"
#include "include/primitive.h"
#include "include/packet.h"
#include "include/disconnect.h"
#include "include/network_connection.h"
#include "include/simpleq_entry.h"
#include "include/string_z.h"

// Data link filenames
#define L_READ "L_lec"
#define L_WRITE "L_ecr"

#define CONNECTIONS_LENGTH 256
#define RESEND_ATTEMPTS 1

// Global declarations
struct network_connection* connections[CONNECTIONS_LENGTH];
pthread_mutex_t connections_mutex[CONNECTIONS_LENGTH];

ssize_t connections_id_lookup[CONNECTIONS_LENGTH];
pthread_mutex_t connections_lookup_mutex[CONNECTIONS_LENGTH];

SIMPLEQ_HEAD(listhead, simpleq_entry) head = SIMPLEQ_HEAD_INITIALIZER(head);

int transport_fifo_fd;
pthread_mutex_t transport_write_mutex;
pthread_mutex_t data_link_write_mutex;
pthread_mutex_t data_link_read_mutex;

// Function prototypes
void read_from_transport();

void write_to_transport(void* data, size_t length);
void write_to_data_link(void* data, size_t length);
void read_from_data_link(void* data, size_t length);

ssize_t get_connection_by_id(unsigned char id);
void destroy_connection(unsigned char connection);

void handle_connect_req_primitive(struct connect_req_primitive* connect_req_primitive);
void handle_data_req_primitive(struct data_req_primitive* data_req_primitive);
void handle_disconnect_req_primitive(struct disconnect_req_primitive* disconnect_req_primitive);

void handle_data_packet(struct data_packet* data_packet, unsigned char* pr, unsigned char resend);
void handle_connect_packet(struct connect_packet* connect_packet);

int main() {
    // Initialize random seed
    srand(time(NULL));

    // Mutex and lookup initalization
    for (size_t i = 0; i < CONNECTIONS_LENGTH; i++) {
        pthread_mutex_init(&connections_mutex[i], NULL);
        pthread_mutex_init(&connections_lookup_mutex[i], NULL);
        connections_id_lookup[i] = -1;
    }
    pthread_mutex_init(&transport_write_mutex, NULL);
    pthread_mutex_init(&data_link_write_mutex, NULL);
    pthread_mutex_init(&data_link_read_mutex, NULL);

    read_from_transport();

    // Wait for all threads to terminate
    while (!SIMPLEQ_EMPTY(&head)) {
        struct simpleq_entry* entry = SIMPLEQ_FIRST(&head);
        SIMPLEQ_REMOVE_HEAD(&head, entries);
        pthread_join(*(pthread_t*)entry->data, NULL);
        free(entry->data);
        free(entry);
    }

    // Mutex destruction
    for (size_t i = 0; i < CONNECTIONS_LENGTH; i++) {
        pthread_mutex_destroy(&connections_mutex[i]);
        pthread_mutex_destroy(&connections_lookup_mutex[i]);
    }
    pthread_mutex_destroy(&transport_write_mutex);
    pthread_mutex_destroy(&data_link_write_mutex);
    pthread_mutex_destroy(&data_link_read_mutex);
    return 0;
}

void read_from_transport() {
    // Open network FIFO
    int network_fifo_fd = open(NETWORK_FIFO, O_RDONLY);
    if (network_fifo_fd == -1) {
        perror(ERR_NETWORK_FIFO_OPEN);
        return;
    }

    // Open transport FIFO
    transport_fifo_fd = open(TRANSPORT_FIFO, O_WRONLY);
    if (transport_fifo_fd == -1) {
        perror(ERR_TRANSPORT_FIFO_OPEN);
        return -1;
    }

    size_t bytes_read;
    // Create incoming primitive
    struct primitive* primitive = malloc(sizeof(struct primitive));

    // Read primitive from network
    while ((bytes_read = read(network_fifo_fd, primitive, sizeof(struct primitive))) > 0) {
        // Create thread
        pthread_t* tid = NULL;

        switch (primitive->primitive & N_MASK) {
            case N_CONNECT_REQ: {
                // Cast primitive
                struct connect_req_primitive* connect_req_primitive = malloc(sizeof(struct connect_req_primitive));
                memcpy(connect_req_primitive, primitive, sizeof(struct primitive));

                // Read source address
                if ((bytes_read = read(network_fifo_fd, &connect_req_primitive->source, sizeof(unsigned char))) == -1) {
                    free(connect_req_primitive);
                    break;
                }

                // Read dest address
                if ((bytes_read = read(network_fifo_fd, &connect_req_primitive->dest, sizeof(unsigned char))) == -1) {
                    free(connect_req_primitive);
                    break;
                }

                // Create thread
                tid = malloc(sizeof(pthread_t));
                if (pthread_create(tid, NULL, handle_connect_req_primitive, connect_req_primitive) == -1) {
                    free(connect_req_primitive);
                    free(tid);
                    tid = NULL;

                    perror(ERR_PTHREAD_CREATE);
                    break;
                }
                break;
            }
            case N_DATA_REQ: {
                // Cast primitive
                struct data_req_primitive* data_req_primitive = malloc(sizeof(struct data_req_primitive));
                memcpy(data_req_primitive, primitive, sizeof(struct primitive));

                // Read null-terminated data
                data_req_primitive->data = malloc(sizeof(char));
                if ((bytes_read = read_z(network_fifo_fd, &data_req_primitive->data)) == -1) {
                    free(data_req_primitive->data);
                    free(data_req_primitive);
                    break;
                }

                // Create thread
                tid = malloc(sizeof(pthread_t));
                if (pthread_create(tid, NULL, handle_data_req_primitive, data_req_primitive) == -1) {
                    free(data_req_primitive->data);
                    free(data_req_primitive);
                    free(tid);
                    tid = NULL;

                    perror(ERR_PTHREAD_CREATE);
                    break;
                }
                break;
            }
            case N_DISCONNECT_REQ: {
                // Cast primitive
                struct disconnect_req_primitive* disconnect_req_primitive = malloc(sizeof(struct disconnect_req_primitive));
                memcpy(disconnect_req_primitive, primitive, sizeof(struct primitive));

                // Read resp address
                if ((bytes_read = read(network_fifo_fd, &disconnect_req_primitive->resp, sizeof(unsigned char))) == -1) {
                    free(disconnect_req_primitive);
                    break;
                }

                // Create thread
                tid = malloc(sizeof(pthread_t));
                if (pthread_create(tid, NULL, handle_disconnect_req_primitive, disconnect_req_primitive) == -1) {
                    free(disconnect_req_primitive);
                    free(tid);
                    tid = NULL;

                    perror(ERR_PTHREAD_CREATE);
                    break;
                }
                break;
            }
        }

        if (tid) {
            // Create queue entry
            struct simpleq_entry* tid_entry = malloc(sizeof(struct simpleq_entry));
            tid_entry->data = tid;

            // Insert entry to queue
            if (SIMPLEQ_EMPTY(&head)) {
                SIMPLEQ_INSERT_HEAD(&head, tid_entry, entries);
            } else {
                SIMPLEQ_INSERT_TAIL(&head, tid_entry, entries);
            }
        }
    }

    if (bytes_read == -1) {
        perror(ERR_NETWORK_FIFO_READ);
    }

    // Close transport FIFO
    close(transport_fifo_fd);

    free(primitive);
    close(network_fifo_fd);
}

void write_to_transport(void* data, size_t length) {
    ssize_t bytes_wrote = write(transport_fifo_fd, data, length);
    if (bytes_wrote == -1) {
        perror(ERR_TRANSPORT_FIFO_WRITE);
    }
}

void write_to_data_link(void* data, size_t length) {
    ssize_t bytes_wrote;
    int l_write_fd = open(L_WRITE, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (l_write_fd == -1) {
        perror(ERR_L_WRITE_OPEN);
    } else {
        bytes_wrote = write(l_write_fd, data, length);
        if (bytes_wrote == -1) {
            perror(ERR_L_WRITE_WRITE);
        }
        close(l_write_fd);
    }
}

void read_from_data_link(void* data, size_t length) {
    ssize_t bytes_wrote;
    int l_read_fd = open(L_READ, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (l_read_fd == -1) {
        perror(ERR_L_READ_OPEN);
    } else {
        bytes_wrote = write(l_read_fd, data, length);
        if (bytes_wrote == -1) {
            perror(ERR_L_READ_WRITE);
        }
        close(l_read_fd);
    }
}

ssize_t get_connection_by_id(unsigned char id) {
    // Use a lookup table for better synchronization of conccurent applications
    ssize_t connection = connections_id_lookup[id];
    if (connection != -1) {
        pthread_mutex_lock(&connections_mutex[connection]);
    }
    return connection;
}

void destroy_connection(unsigned char connection) {
    connections_id_lookup[connections[connection]->id] = -1;
    free(connections[connection]);
    connections[connection] = NULL;
}

void handle_connect_req_primitive(struct connect_req_primitive* connect_req_primitive) {
    pthread_mutex_lock(&connections_lookup_mutex[connect_req_primitive->id]);
    size_t connection_index = get_connection_by_id(connect_req_primitive->id);

    // Ignore if connection already exists
    if (connection_index == -1) {
        // Find random available connection number
        connection_index = rand() % CONNECTIONS_LENGTH;
        pthread_mutex_lock(&connections_mutex[connection_index]);
        while (connections[connection_index]) {
            pthread_mutex_unlock(&connections_mutex[connection_index]);
            connection_index = rand() % CONNECTIONS_LENGTH;
            pthread_mutex_lock(&connections_mutex[connection_index]);
        }

        // Create connection
        struct network_connection* connection = malloc(sizeof(struct network_connection));
        connection->id = connect_req_primitive->id;
        connection->source = connect_req_primitive->source;
        connection->dest = connect_req_primitive->dest;
        connection->established = 0;

        // Set connection
        connections[connection_index] = connection;
        connections_id_lookup[connect_req_primitive->id] = connection_index;

        // Create connect packet
        struct connect_packet* connect_packet = malloc(sizeof(struct connect_packet));
        connect_packet->connection = connection_index;
        connect_packet->type = T_CONNECT;
        connect_packet->source = connections[connection_index]->source;
        connect_packet->dest = connections[connection_index]->dest;

        handle_connect_packet(connect_packet);
        free(connect_packet);

        pthread_mutex_unlock(&connections_mutex[connection_index]);
    }

    pthread_mutex_unlock(&connections_lookup_mutex[connect_req_primitive->id]);
    free(connect_req_primitive);
}

void handle_data_req_primitive(struct data_req_primitive* data_req_primitive) {
    pthread_mutex_lock(&connections_lookup_mutex[data_req_primitive->id]);
    ssize_t connection = get_connection_by_id(data_req_primitive->id);

    // Ignore primitive if connection does not exist or is not established
    if (connection != -1) {
        if (connections[connection]->established) {
            size_t data_length = strlen(data_req_primitive->data) + 1;
            size_t data_bytes_sent = 0;
            unsigned char ps = 0;
            unsigned char pr = 0;

            while (ps == pr && data_length - data_bytes_sent > MAX_DATA_LENGTH) {
                // Create segmented data packet
                struct data_packet* data_packet = malloc(sizeof(struct data_packet));
                data_packet->connection = connection;
                data_packet->type = T_DATA | M_MASK | (ps << PS_RSH);
                data_packet->data = malloc(MAX_DATA_LENGTH);
                memcpy(data_packet->data, data_req_primitive->data + data_bytes_sent, MAX_DATA_LENGTH);

                // Increment bytes sent and PS
                data_bytes_sent += MAX_DATA_LENGTH;
                ps = (ps + 1) % PS_MOD;

                handle_data_packet(data_packet, &pr, RESEND_ATTEMPTS);
                free(data_packet->data);
                free(data_packet);
            }

            if (ps == pr) {
                // Create last data packet
                struct data_packet* data_packet = malloc(sizeof(struct data_packet));
                data_packet->connection = connection;
                data_packet->type = T_DATA | (ps << PS_RSH);
                data_packet->data = malloc(data_length - data_bytes_sent);
                memcpy(data_packet->data, data_req_primitive->data + data_bytes_sent, data_length - data_bytes_sent);

                handle_data_packet(data_packet, &pr, RESEND_ATTEMPTS);
                free(data_packet->data);
                free(data_packet);
            }
        }

        pthread_mutex_unlock(&connections_mutex[connection]);
    }

    pthread_mutex_unlock(&connections_lookup_mutex[data_req_primitive->id]);
    free(data_req_primitive->data);
    free(data_req_primitive);
}

void handle_disconnect_req_primitive(struct disconnect_req_primitive* disconnect_req_primitive) {
    pthread_mutex_lock(&connections_lookup_mutex[disconnect_req_primitive->id]);
    ssize_t connection = get_connection_by_id(disconnect_req_primitive->id);

    // Ignore primitive if connection does not exist
    if (connection != -1) {
        // Create disconnect packet
        struct disconnect_packet* disconnect_packet = malloc(sizeof(struct disconnect_packet));
        disconnect_packet->connection = connection;
        disconnect_packet->type = T_DISCONNECT;
        disconnect_packet->source = connections[connection]->source;
        disconnect_packet->dest = connections[connection]->dest;
        disconnect_packet->reason = DISCONNECT_T;

        // Write to data link in a thread-safe way
        pthread_mutex_lock(&data_link_write_mutex);
        write_to_data_link(disconnect_packet, sizeof(struct disconnect_packet));
        pthread_mutex_unlock(&data_link_write_mutex);
        free(disconnect_packet);

        destroy_connection(connection);
        pthread_mutex_unlock(&connections_mutex[connection]);
    }

    pthread_mutex_unlock(&connections_lookup_mutex[disconnect_req_primitive->id]);
    free(disconnect_req_primitive);
}

void handle_data_packet(struct data_packet* data_packet, unsigned char* pr, unsigned char resend) {
    // Copy data packet to buffer to remove struct 8-byte padding
    size_t data_length = ((data_packet->type & M_MASK) >> M_RSH) ? MAX_DATA_LENGTH : strlen(data_packet->data) + 1;
    char* buffer = malloc(sizeof(unsigned char) + sizeof(unsigned char) + data_length);
    size_t buffer_length = 0;

    memcpy(buffer + buffer_length, &data_packet->connection, sizeof(unsigned char));
    buffer_length += sizeof(unsigned char);

    memcpy(buffer + buffer_length, &data_packet->type, sizeof(unsigned char));
    buffer_length += sizeof(unsigned char);

    memcpy(buffer + buffer_length, data_packet->data, data_length);
    buffer_length += data_length;

    // Write buffer to data link in a thread-safe way
    pthread_mutex_lock(&data_link_write_mutex);
    write_to_data_link(buffer, buffer_length);
    pthread_mutex_unlock(&data_link_write_mutex);
    free(buffer);

    if (connections[data_packet->connection]->source % 15 == 0) {
        // No response, resend
        if (resend) {
            handle_data_packet(data_packet, pr, resend - 1);
        }
    } else if (((data_packet->type & PS_MASK) >> PS_RSH) == (rand() % PS_MOD)) {
        // Create NACK packet, resend
        struct packet* nack_packet = malloc(sizeof(struct packet));
        nack_packet->connection = data_packet->connection;
        nack_packet->type = T_NACK | (*pr << PR_RSH);

        // Write to data link read in thread-safe way
        pthread_mutex_lock(&data_link_read_mutex);
        read_from_data_link(nack_packet, sizeof(struct packet));
        pthread_mutex_unlock(&data_link_read_mutex);
        free(nack_packet);

        if (resend) {
            handle_data_packet(data_packet, pr, resend - 1);
        }
    } else {
        // Increment PR
        *pr = (*pr + 1) % PS_MOD;

        // Create ACK packet
        struct packet* ack_packet = malloc(sizeof(struct packet));
        ack_packet->connection = data_packet->connection;
        ack_packet->type = T_ACK | (*pr << PR_RSH);

        // Write to data link read in thread-safe way
        pthread_mutex_lock(&data_link_read_mutex);
        read_from_data_link(ack_packet, sizeof(struct packet));
        pthread_mutex_unlock(&data_link_read_mutex);
        free(ack_packet);
    }
}

void handle_connect_packet(struct connect_packet* connect_packet) {
    // Write to data link in a thread-safe way
    pthread_mutex_lock(&data_link_write_mutex);
    write_to_data_link(connect_packet, sizeof(struct connect_packet));
    pthread_mutex_unlock(&data_link_write_mutex);

    if (connect_packet->source % 19 == 0) {
        // No response, create disconnect primitive
        struct disconnect_ind_primitive* disconnect_ind_primitive = malloc(sizeof(struct disconnect_ind_primitive));
        disconnect_ind_primitive->id = connections[connect_packet->connection]->id;
        disconnect_ind_primitive->primitive = N_DISCONNECT_IND;
        disconnect_ind_primitive->resp = connect_packet->dest;
        disconnect_ind_primitive->reason = DISCONNECT_R;

        // Destroy connection
        destroy_connection(connect_packet->connection);

        // Write to transport in a thread-safe way
        pthread_mutex_lock(&transport_write_mutex);
        write_to_transport(disconnect_ind_primitive, sizeof(struct disconnect_ind_primitive));
        pthread_mutex_unlock(&transport_write_mutex);
        free(disconnect_ind_primitive);
    } else if (connect_packet->source % 13 == 0) {
        // Create response packet
        struct disconnect_packet* disconnect_packet = malloc(sizeof(struct disconnect_packet));
        disconnect_packet->connection = connect_packet->connection;
        disconnect_packet->type = T_DISCONNECT;
        disconnect_packet->source = connect_packet->dest;
        disconnect_packet->dest = connect_packet->source;
        disconnect_packet->reason = DISCONNECT_T;

        // Write to data link read in thread-safe way
        pthread_mutex_lock(&data_link_read_mutex);
        read_from_data_link(disconnect_packet, sizeof(struct disconnect_packet));
        pthread_mutex_unlock(&data_link_read_mutex);

        // Create disconnect primitive from packet
        struct disconnect_ind_primitive* disconnect_ind_primitive = malloc(sizeof(struct disconnect_ind_primitive));
        disconnect_ind_primitive->id = connections[disconnect_packet->connection]->id;
        disconnect_ind_primitive->primitive = N_DISCONNECT_IND;
        disconnect_ind_primitive->resp = disconnect_packet->source;
        disconnect_ind_primitive->reason = disconnect_packet->reason;

        // Destroy connection
        destroy_connection(disconnect_packet->connection);
        free(disconnect_packet);

        // Write to transport in a thread-safe way
        pthread_mutex_lock(&transport_write_mutex);
        write_to_transport(disconnect_ind_primitive, sizeof(struct disconnect_ind_primitive));
        pthread_mutex_unlock(&transport_write_mutex);
        free(disconnect_ind_primitive);
    } else {
        // Create response packet
        struct connect_packet* response_packet = malloc(sizeof(struct connect_packet));
        response_packet->connection = connect_packet->connection;
        response_packet->type = T_ESTABLISHED;
        response_packet->source = connect_packet->dest;
        response_packet->dest = connect_packet->source;

        // Write to data link read in thread-safe way
        pthread_mutex_lock(&data_link_read_mutex);
        read_from_data_link(response_packet, sizeof(struct connect_packet));
        pthread_mutex_unlock(&data_link_read_mutex);

        // Create connect conf primitive from packet
        struct connect_conf_primitive* connect_conf_primitive = malloc(sizeof(struct connect_conf_primitive));
        connect_conf_primitive->id = connections[response_packet->connection]->id;
        connect_conf_primitive->primitive = N_CONNECT_CONF;
        connect_conf_primitive->resp = response_packet->dest;

        // Set connection established
        connections[response_packet->connection]->established = 1;
        free(response_packet);

        // Write to transport in a thread-safe way
        pthread_mutex_lock(&transport_write_mutex);
        write_to_transport(connect_conf_primitive, sizeof(struct connect_conf_primitive));
        pthread_mutex_unlock(&transport_write_mutex);
        free(connect_conf_primitive);
    }
}
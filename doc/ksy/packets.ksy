meta:
  id: packets
  endian: be
seq:
  - id: packets
    type: packet_type
    repeat: eos
types:
  packet_type:
    seq:
      - id: connection
        type: u1
      - id: type
        type: u1
        enum: packet_type_enum
      - id: source
        type: u1
        if: type == packet_type_enum::t_connect or type == packet_type_enum::t_established or type == packet_type_enum::t_disconnect
      - id: dest
        type: u1
        if: type == packet_type_enum::t_connect or type == packet_type_enum::t_established or type == packet_type_enum::t_disconnect
      - id: reason
        type: u1
        enum: disconnect_enum
        if: type == packet_type_enum::t_disconnect
      - id: data
        type: str
        size: 128
        encoding: ASCII
        if: (type.to_i & 1) == 0 and ((type.to_i & 16) >> 4) == 1
      - id: data_z
        type: str
        terminator: 0
        encoding: ASCII
        if: (type.to_i & 1) == 0 and ((type.to_i & 16) >> 4) == 0
    instances:
      is_data:
        value: (type.to_i & 1) == 0
      is_ack:
        value: (type.to_i & 15) == packet_type_enum::t_ack.to_i
      is_nack:
        value: (type.to_i & 15) == packet_type_enum::t_nack.to_i
      segmentation:
        value: ((type.to_i & 16) >> 4)
      ps:
        value: ((type.to_i & 14) >> 1)
      pr:
        value: ((type.to_i & 224) >> 5)
enums:
  packet_type_enum:
    0: t_data
    11: t_connect
    15: t_established
    19: t_disconnect
    1: t_ack
    9: t_nack
  disconnect_enum:
    1: disconnect_t
    2: disconnect_r

meta:
  id: primitives
  endian: be
seq:
  - id: primitives
    type: primitive_type
    repeat: eos
types:
  primitive_type:
    seq:
      - id: id
        type: u1
      - id: primitive
        type: u1
        enum: primitive_enum
      - id: source
        type: u1
        if: primitive == primitive_enum::n_connect_req or primitive == primitive_enum::n_connect_ind
      - id: dest
        type: u1
        if: primitive == primitive_enum::n_connect_req or primitive == primitive_enum::n_connect_ind
      - id: resp
        type: u1
        if: primitive == primitive_enum::n_connect_resp or primitive == primitive_enum::n_connect_conf or primitive == primitive_enum::n_disconnect_req or primitive == primitive_enum::n_disconnect_ind
      - id: data
        type: str
        terminator: 0
        encoding: ASCII
        if: primitive == primitive_enum::n_data_req or primitive == primitive_enum::n_data_ind
      - id: reason
        type: u1
        enum: disconnect_enum
        if: primitive == primitive_enum::n_disconnect_ind
enums:
  primitive_enum:
    0: n_connect_req
    1: n_connect_ind
    2: n_connect_resp
    3: n_connect_conf
    4: n_data_req
    5: n_data_ind
    6: n_disconnect_req
    7: n_disconnect_ind
  disconnect_enum:
    1: disconnect_t
    2: disconnect_r
    
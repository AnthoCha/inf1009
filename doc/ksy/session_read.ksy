meta:
  id: session_read
  endian: be
seq:
  - id: session_read
    type: app_data
    repeat: eos
types:
  app_data:
    seq:
      - id: id
        type: u1
      - id: data
        type: str
        terminator: 0
        encoding: ASCII
meta:
  id: session_write
  endian: be
seq:
  - id: session_write
    type: app_response
    repeat: eos
types:
  app_response:
    seq:
      - id: id
        type: u1
      - id: type
        type: u1
        enum: response_type_enum
      - id: data
        type: str
        terminator: 0
        encoding: ASCII
        if: type == response_type_enum::s_data
      - id: reason
        type: u1
        enum: disconnect_enum
        if: type == response_type_enum::s_disconnect
enums:
  response_type_enum:
    0: s_data
    1: s_disconnect
  disconnect_enum:
    1: disconnect_t
    2: disconnect_r
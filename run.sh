./clean.sh
make
mkdir -p /tmp/inf1009
mkfifo /tmp/inf1009/network.fifo
mkfifo /tmp/inf1009/transport.fifo
./bin/network &
./bin/transport &
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
wait
